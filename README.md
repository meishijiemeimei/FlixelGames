# FlixelGames
It is mini games having been made by HaxeFlixel

## SuperPuzzleRPG (2016/3/13)
Puzzle RPG -> [SuperPuzzleRPG](http://haxeflixel.2dgames.jp/index.php?Product%2FSuperPuzzleRPG)

## ActiveTD (2016/3/9)
Platforme Tower Defense -> [ActiveTD](http://haxeflixel.2dgames.jp/index.php?Product%2FActiveTD)

## R.G.B. Guardian (2016/3/3)
Color switching action -> [R.G.B. Guardian](http://haxeflixel.2dgames.jp/index.php?Product%2FRGBGuardian)

## HyperSneakingMission (2016/2/28)
Auto-Scrolling Stealth -> [HyperSneakingMission](http://haxeflixel.2dgames.jp/index.php?Product%2FHyperSneakingMission)

## HellBall (2016/2/23)
Billiards -> [HellBall](http://haxeflixel.2dgames.jp/index.php?Product%2FHellBall)

## TwinFlip (2016/2/18)
Player switching action puzzle -> [TwinFlip](http://haxeflixel.2dgames.jp/index.php?Product%2FTwinFlip)

## Room666 (2016/2/14)
Escape action -> [Room666](http://haxeflixel.2dgames.jp/index.php?Product%2FRoom666)

## BasementShooter2 (2016/2/11)
2D Platformer -> [BasementShooter2](http://haxeflixel.2dgames.jp/index.php?Product%2FBasementShooter2)


## BasementShooter (2016/2/4)
2D Platformer -> [BasementShooter](http://haxeflixel.2dgames.jp/index.php?Product%2FBasementShooter)

## HyperRacer2015 (2015/9/21)
Instant death Racing Game ->  [HyperRacer2015](http://haxeflixel.2dgames.jp/index.php?Product%2FHyperRacer2015)
